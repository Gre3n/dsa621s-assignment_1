import ballerina/grpc;
import ballerina/protobuf.types.wrappers;

public isolated client class assessmentMgtClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR_SCHOOL, getDescriptorMapSchool());
    }

    isolated remote function create_courses(courseinfo|ContextCourseinfo req) returns string|grpc:Error {
        map<string|string[]> headers = {};
        courseinfo message;
        if req is ContextCourseinfo {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("grpc_service.assessmentMgt/create_courses", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function create_coursesContext(courseinfo|ContextCourseinfo req) returns wrappers:ContextString|grpc:Error {
        map<string|string[]> headers = {};
        courseinfo message;
        if req is ContextCourseinfo {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("grpc_service.assessmentMgt/create_courses", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function assign_courses(courseinfo|ContextCourseinfo req) returns string|grpc:Error {
        map<string|string[]> headers = {};
        courseinfo message;
        if req is ContextCourseinfo {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("grpc_service.assessmentMgt/assign_courses", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function assign_coursesContext(courseinfo|ContextCourseinfo req) returns wrappers:ContextString|grpc:Error {
        map<string|string[]> headers = {};
        courseinfo message;
        if req is ContextCourseinfo {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("grpc_service.assessmentMgt/assign_courses", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function create_admin(Admin|ContextAdmin req) returns string|grpc:Error {
        map<string|string[]> headers = {};
        Admin message;
        if req is ContextAdmin {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("grpc_service.assessmentMgt/create_admin", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function create_adminContext(Admin|ContextAdmin req) returns wrappers:ContextString|grpc:Error {
        map<string|string[]> headers = {};
        Admin message;
        if req is ContextAdmin {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("grpc_service.assessmentMgt/create_admin", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function create_assessos(Assessor|ContextAssessor req) returns string|grpc:Error {
        map<string|string[]> headers = {};
        Assessor message;
        if req is ContextAssessor {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("grpc_service.assessmentMgt/create_assessos", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function create_assessosContext(Assessor|ContextAssessor req) returns wrappers:ContextString|grpc:Error {
        map<string|string[]> headers = {};
        Assessor message;
        if req is ContextAssessor {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("grpc_service.assessmentMgt/create_assessos", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function create_learner(learner|ContextLearner req) returns string|grpc:Error {
        map<string|string[]> headers = {};
        learner message;
        if req is ContextLearner {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("grpc_service.assessmentMgt/create_learner", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function create_learnerContext(learner|ContextLearner req) returns wrappers:ContextString|grpc:Error {
        map<string|string[]> headers = {};
        learner message;
        if req is ContextLearner {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("grpc_service.assessmentMgt/create_learner", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function submit_assignments(assignmentinfo|ContextAssignmentinfo req) returns string|grpc:Error {
        map<string|string[]> headers = {};
        assignmentinfo message;
        if req is ContextAssignmentinfo {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("grpc_service.assessmentMgt/submit_assignments", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function submit_assignmentsContext(assignmentinfo|ContextAssignmentinfo req) returns wrappers:ContextString|grpc:Error {
        map<string|string[]> headers = {};
        assignmentinfo message;
        if req is ContextAssignmentinfo {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("grpc_service.assessmentMgt/submit_assignments", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function request_assignments(string|wrappers:ContextString req) returns string|grpc:Error {
        map<string|string[]> headers = {};
        string message;
        if req is wrappers:ContextString {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("grpc_service.assessmentMgt/request_assignments", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function request_assignmentsContext(string|wrappers:ContextString req) returns wrappers:ContextString|grpc:Error {
        map<string|string[]> headers = {};
        string message;
        if req is wrappers:ContextString {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("grpc_service.assessmentMgt/request_assignments", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function submit_marks(assignmentinfo|ContextAssignmentinfo req) returns string|grpc:Error {
        map<string|string[]> headers = {};
        assignmentinfo message;
        if req is ContextAssignmentinfo {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("grpc_service.assessmentMgt/submit_marks", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function submit_marksContext(assignmentinfo|ContextAssignmentinfo req) returns wrappers:ContextString|grpc:Error {
        map<string|string[]> headers = {};
        assignmentinfo message;
        if req is ContextAssignmentinfo {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("grpc_service.assessmentMgt/submit_marks", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function register(courseinfo|ContextCourseinfo req) returns string|grpc:Error {
        map<string|string[]> headers = {};
        courseinfo message;
        if req is ContextCourseinfo {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("grpc_service.assessmentMgt/register", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function registerContext(courseinfo|ContextCourseinfo req) returns wrappers:ContextString|grpc:Error {
        map<string|string[]> headers = {};
        courseinfo message;
        if req is ContextCourseinfo {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("grpc_service.assessmentMgt/register", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }
}

public client class AssessmentMgtStringCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendString(string response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextString(wrappers:ContextString response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextLearner record {|
    learner content;
    map<string|string[]> headers;
|};

public type ContextCourseinfo record {|
    courseinfo content;
    map<string|string[]> headers;
|};

public type ContextAssignmentinfo record {|
    assignmentinfo content;
    map<string|string[]> headers;
|};

public type ContextAdmin record {|
    Admin content;
    map<string|string[]> headers;
|};

public type ContextAssessor record {|
    Assessor content;
    map<string|string[]> headers;
|};

public type learner record {|
    string learnerId = "";
    string userFname = "";
    string userLname = "";
    string[] Courses = [];
|};

public type courseinfo record {|
    string courseId = "";
    string courseName = "";
    string assessor = "";
    int NOassignment = 0;
    int assignmentWeight = 0;
|};

public type assignmentinfo record {|
    string learnerID = "";
    string courseId = "";
    int assingmentNumber = 0;
    int assignmentMark = 0;
    string assessor = "";
|};

public type Admin record {|
    string adminId = "";
    string userFname = "";
    string userLname = "";
    string userRole = "";
|};

public type Assessor record {|
    string assessorId = "";
    string userFname = "";
    string userLname = "";
    string userRole = "";
|};

const string ROOT_DESCRIPTOR_SCHOOL = "0A0C7363686F6F6C2E70726F746F120C677270635F736572766963651A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F22790A0541646D696E12180A0761646D696E4964180120012809520761646D696E4964121C0A0975736572466E616D65180220012809520975736572466E616D65121C0A09757365724C6E616D651803200128095209757365724C6E616D65121A0A0875736572526F6C65180420012809520875736572526F6C652282010A084173736573736F72121E0A0A6173736573736F724964180120012809520A6173736573736F724964121C0A0975736572466E616D65180220012809520975736572466E616D65121C0A09757365724C6E616D651803200128095209757365724C6E616D65121A0A0875736572526F6C65180420012809520875736572526F6C65227D0A076C6561726E6572121C0A096C6561726E6572496418012001280952096C6561726E65724964121C0A0975736572466E616D65180220012809520975736572466E616D65121C0A09757365724C6E616D651803200128095209757365724C6E616D6512180A07436F75727365731804200328095207436F757273657322B4010A0A636F75727365696E666F121A0A08636F7572736549641801200128095208636F757273654964121E0A0A636F757273654E616D65180220012809520A636F757273654E616D65121A0A086173736573736F7218032001280952086173736573736F7212220A0C4E4F61737369676E6D656E74180420012805520C4E4F61737369676E6D656E74122A0A1061737369676E6D656E74576569676874180520012805521061737369676E6D656E7457656967687422BA010A0E61737369676E6D656E74696E666F121C0A096C6561726E6572494418012001280952096C6561726E65724944121A0A08636F7572736549641802200128095208636F757273654964122A0A10617373696E676D656E744E756D6265721803200128055210617373696E676D656E744E756D62657212260A0E61737369676E6D656E744D61726B180420012805520E61737369676E6D656E744D61726B121A0A086173736573736F7218052001280952086173736573736F7232AB050A0D6173736573736D656E744D677412480A0E6372656174655F636F757273657312182E677270635F736572766963652E636F75727365696E666F1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512480A0E61737369676E5F636F757273657312182E677270635F736572766963652E636F75727365696E666F1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512410A0C6372656174655F61646D696E12132E677270635F736572766963652E41646D696E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512470A0F6372656174655F6173736573736F7312162E677270635F736572766963652E4173736573736F721A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512450A0E6372656174655F6C6561726E657212152E677270635F736572766963652E6C6561726E65721A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512500A127375626D69745F61737369676E6D656E7473121C2E677270635F736572766963652E61737369676E6D656E74696E666F1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512510A13726571756573745F61737369676E6D656E7473121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565124A0A0C7375626D69745F6D61726B73121C2E677270635F736572766963652E61737369676E6D656E74696E666F1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512420A08726567697374657212182E677270635F736572766963652E636F75727365696E666F1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565620670726F746F33";

public isolated function getDescriptorMapSchool() returns map<string> {
    return {"google/protobuf/wrappers.proto": "0A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F120F676F6F676C652E70726F746F62756622230A0B446F75626C6556616C756512140A0576616C7565180120012801520576616C756522220A0A466C6F617456616C756512140A0576616C7565180120012802520576616C756522220A0A496E74363456616C756512140A0576616C7565180120012803520576616C756522230A0B55496E74363456616C756512140A0576616C7565180120012804520576616C756522220A0A496E74333256616C756512140A0576616C7565180120012805520576616C756522230A0B55496E74333256616C756512140A0576616C756518012001280D520576616C756522210A09426F6F6C56616C756512140A0576616C7565180120012808520576616C756522230A0B537472696E6756616C756512140A0576616C7565180120012809520576616C756522220A0A427974657356616C756512140A0576616C756518012001280C520576616C756542570A13636F6D2E676F6F676C652E70726F746F627566420D577261707065727350726F746F50015A057479706573F80101A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33", "school.proto": "0A0C7363686F6F6C2E70726F746F120C677270635F736572766963651A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F22790A0541646D696E12180A0761646D696E4964180120012809520761646D696E4964121C0A0975736572466E616D65180220012809520975736572466E616D65121C0A09757365724C6E616D651803200128095209757365724C6E616D65121A0A0875736572526F6C65180420012809520875736572526F6C652282010A084173736573736F72121E0A0A6173736573736F724964180120012809520A6173736573736F724964121C0A0975736572466E616D65180220012809520975736572466E616D65121C0A09757365724C6E616D651803200128095209757365724C6E616D65121A0A0875736572526F6C65180420012809520875736572526F6C65227D0A076C6561726E6572121C0A096C6561726E6572496418012001280952096C6561726E65724964121C0A0975736572466E616D65180220012809520975736572466E616D65121C0A09757365724C6E616D651803200128095209757365724C6E616D6512180A07436F75727365731804200328095207436F757273657322B4010A0A636F75727365696E666F121A0A08636F7572736549641801200128095208636F757273654964121E0A0A636F757273654E616D65180220012809520A636F757273654E616D65121A0A086173736573736F7218032001280952086173736573736F7212220A0C4E4F61737369676E6D656E74180420012805520C4E4F61737369676E6D656E74122A0A1061737369676E6D656E74576569676874180520012805521061737369676E6D656E7457656967687422BA010A0E61737369676E6D656E74696E666F121C0A096C6561726E6572494418012001280952096C6561726E65724944121A0A08636F7572736549641802200128095208636F757273654964122A0A10617373696E676D656E744E756D6265721803200128055210617373696E676D656E744E756D62657212260A0E61737369676E6D656E744D61726B180420012805520E61737369676E6D656E744D61726B121A0A086173736573736F7218052001280952086173736573736F7232AB050A0D6173736573736D656E744D677412480A0E6372656174655F636F757273657312182E677270635F736572766963652E636F75727365696E666F1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512480A0E61737369676E5F636F757273657312182E677270635F736572766963652E636F75727365696E666F1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512410A0C6372656174655F61646D696E12132E677270635F736572766963652E41646D696E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512470A0F6372656174655F6173736573736F7312162E677270635F736572766963652E4173736573736F721A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512450A0E6372656174655F6C6561726E657212152E677270635F736572766963652E6C6561726E65721A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512500A127375626D69745F61737369676E6D656E7473121C2E677270635F736572766963652E61737369676E6D656E74696E666F1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512510A13726571756573745F61737369676E6D656E7473121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565124A0A0C7375626D69745F6D61726B73121C2E677270635F736572766963652E61737369676E6D656E74696E666F1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512420A08726567697374657212182E677270635F736572766963652E636F75727365696E666F1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565620670726F746F33"};
}

