import ballerina/grpc;
import ballerina/log;

listener grpc:Listener ep = new (9090);

type response record {
};
courseinfo [] courseList = [];
Admin [] adminList = [];
Assessor [] assessorList = [];
learner [] learnerList = [];
assignmentinfo [] assignmentList = [];

@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR_SCHOOL, descMap: getDescriptorMapSchool()}
service "assessmentMgt" on ep {
    remote function create_courses(courseinfo value) returns string|error {
        log:printInfo(value.courseName);
        courseList.push(value);
        return "Successfully added "+value.courseName.toString();

    }

    remote function assign_courses(courseinfo value) returns string|error {
        log:printInfo(value.assessor);
        courseList.push(value);
        return "Successfully added "+value.assessor.toString();
    }

    remote function create_admin(Admin value) returns string|error {
        log:printInfo(value.adminId);
        adminList.push(value);
        return "Successfully added "+value.adminId.toString();
    }

    remote function create_assessors(Assessor value) returns string|error {
        log:printInfo(value.assessorId);
        assessorList.push(value);
        return "Successfully added "+value.assessorId.toString();
    }
    
    remote function create_learner(learner value) returns string|error {
        log:printInfo(value.learnerId);
        learnerList.push(value);
        return "Successfully added "+value.learnerId.toString();
    }

    remote function submit_assignments(assignmentinfo value) returns string|error {
        log:printInfo(value.courseId);
        assignmentList.push(value);
        return "Successfully added "+value.courseId.toString();
    }

    remote function request_assignments(string value) returns string|error {
        return assignmentList.toString();
    }

    remote function submit_marks(assignmentinfo value) returns string|error {
        log:printInfo(value.assignmentMark);
        assignmentList.push(value);
        return "Successfully added "+value.assignmentMark.toString();
    }

    remote function register(learner value) returns string|error {
     log:printInfo(value.Courses);
        learnerList.push(value);
        return "Successfully added "+value.Courses.toString();
}
}

