import ballerina/graphql;
public type CovidStat record {|
    readonly string RegionCode;
    string date?;
    string region;
    decimal deaths?;
    decimal confirmed_cases?;
    decimal recoveries?;
    decimal tested?;
|};

table<CovidStat> key(RegionCode) covidStatsTable = table [
    {RegionCode:"KHMS", date: "12/09/2021", region: "Khomas", deaths:  39, confirmed_cases: 465,  recoveries: 67, tested: 1200},
    {RegionCode:"HDRP", date: "12/09/2021", region: "Hardap", deaths:  15, confirmed_cases: 109,  recoveries: 23, tested: 400}
];


public distinct service class CovidInfo {
    private final readonly & CovidStat entryRecord;

    function init(CovidStat entryRecord) {
        self.entryRecord = entryRecord.cloneReadOnly();
    }

    resource function get RegionCode() returns string {
        return self.entryRecord.RegionCode;
    }

    resource function get region() returns string {
        return self.entryRecord.region;
    }

    resource function get deaths() returns decimal? {
        if self.entryRecord.deaths is decimal {
            return self.entryRecord.deaths;
        }
        return;
    }

    resource function get confirmed_cases() returns decimal? {
        if self.entryRecord.confirmed_cases is decimal {
            return self.entryRecord.confirmed_cases ;
        }
        return;
    }

    resource function get recoveries() returns decimal? {
        if self.entryRecord.recoveries is decimal {
            return self.entryRecord.recoveries;
        }
        return;
    }

    resource function get tested() returns decimal? {
        if self.entryRecord.tested is decimal {
            return self.entryRecord.tested;
        }
        return;
    }
}

service /covid19 on new graphql:Listener(9000) {
    resource function get all() returns CovidInfo[] {
        CovidStat[] covidEntries = covidStatsTable.toArray().cloneReadOnly();
        return covidEntries.map(entry => new CovidInfo(entry));
    }
    
    resource function get filter(string RegionCode) returns CovidInfo? {
    CovidStat? covidStat = covidStatsTable[RegionCode];
    if covidStat is CovidStat {
        return new (covidStat);
    }
    return;
    }

    remote function add(CovidStat entry) returns CovidInfo {
    covidStatsTable.add(entry);
    return new CovidInfo(entry);
}
}




