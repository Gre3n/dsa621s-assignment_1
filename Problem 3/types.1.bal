public type Students record {
    # the studentid of the student
    string studentid?;
    # the first name of the student
    string firstname?;
    # the last name of the student
    string lastname?;
    # the email address of the student
    string email?;
    # the course of the student
    record {}[] Course?;
};
