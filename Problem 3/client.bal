import ballerina/http;

public type Students record {
    # the studentid of the student
    string studentid?;
    # the first name of the student
    string firstname?;
    # the last name of the student
    string lastname?;
    # the email address of the student
    string email?;
    # the course of the student
    record {}[] Course?;
};

# student api for assignment 1
public isolated client class Client {
    final http:Client clientEp;
    # Gets invoked to initialize the `connector`.
    #
    # + clientConfig - The configurations to be used when initializing the `connector` 
    # + serviceUrl - URL of the target service 
    # + return - An error if connector initialization failed 
    public isolated function init(http:ClientConfiguration clientConfig =  {}, string serviceUrl = "https://virtserver.swaggerhub.com/Gre3nLagger/StudenApi/1.0.0") returns error? {
        http:Client httpEp = check new (serviceUrl, clientConfig);
        self.clientEp = httpEp;
        return;
    }
    # return all students
    #
    # + return - invalid input 
    remote isolated function getStudents() returns http:Response|error {
        string resourcePath = string `/student`;
        http:Response response = check self.clientEp->get(resourcePath);
        return response;
    }
    # Add a new student to the student record
    #
    # + return - Invalid input 
    remote isolated function addStudent(Students payload) returns http:Response|error {
        string resourcePath = string `/student`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody, "application/json");
        http:Response response = check self.clientEp->post(resourcePath, request);
        return response;
    }
    # Find student by ID
    #
    # + studentId - ID of Student to return 
    # + return - successful operation 
    remote isolated function getStudentById(int studentId) returns http:Response|error {
        string resourcePath = string `/student/${(studentId)}`;
        http:Response response = check self.clientEp->get(resourcePath);
        return response;
    }
    # Update an existing student
    #
    # + studentId - ID of Student to return 
    # + return - Invalid ID supplied 
    remote isolated function updateStudent(int studentId, Students payload) returns http:Response|error {
        string resourcePath = string `/student/${(studentId)}`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody, "application/json");
        http:Response response = check self.clientEp->put(resourcePath, request);
        return response;
    }
    # Delete an existing Student
    #
    # + studentId - ID of Student to return 
    # + return - Student was successfully deleted 
    remote isolated function deleteStudent(int studentId) returns http:Response|error {
        string resourcePath = string `/student/${(studentId)}`;
        http:Response response = check self.clientEp->delete(resourcePath);
        return response;
    }
}
