//import ballerina/http;

//listener http:Listener ep0 = new (443, config = {host: "virtserver.swaggerhub.com"});
//service /Gre3nLagger/StudenApi/'1\.0\.0 on ep0 {
  
 
   // }
   // resource function post student(@http:Payload Students payload) returns http:MethodNotAllowed {
   // }
  // resource function get student/[int  studentId]() returns http:Ok|http:BadRequest|NotFoundStudents {
  //  }
  //  resource function put student/[int  studentId](@http:Payload Students payload) returns http:BadRequest|http:NotFound|http:MethodNotAllowed {
  //  }
  //  resource function delete student/[int  studentId]() returns http:NoContent {
 //  }

import ballerina/http;
import ballerina/log;
import ballerina/uuid;
 
public type Student record {
    # the studentid of the student
    string studentId?;
    # the first name of the student
    string name;
    # the email address of the student
    string email;
    # the course of the student
    record {}[] Course?;
};
 
# An enum to represent courses
public enum courses {
   Web_development,
   Basic_science,
   Data_networks,
   English


}
 
# Represents courses
public type course record {|
   # course
   string courses;
   
|};
 
# Represents an error
public type Error record {|
   # Error code
   string code;
   # Error message
   string message;
|};
 
# Error response
public type ErrorResponse record {|
   # Error
   Error 'error;
|};
 
# Bad request response
public type ValidationError record {|
   *http:BadRequest;
   # Error response.
   ErrorResponse body;
|};
 
# Represents headers of created response
public type LocationHeader record {|
   # Location header. A link to the created Student record.
   string location;
|};
 
# Student record Creation response
public type StudentCreated record {|
   *http:Created;
   # Location header representing a link to the created Student record.
   LocationHeader headers;
|};
 
# Student updated response
public type StudentUpdated record {|
   *http:Ok;
|};

# The Student service
listener http:Listener ep0 = new (8080, config = {host: "localhost"});
service /vle/api/v1 on ep0 {
    

private   map<Student> students = {};
 
   # List all Students
   # + return - List of Students
    resource function get student() returns Student[] {
       return self.students.toArray();
   }
 
   # Add a new Student
   #
   # + student - student to be added
   # + return - student created response or validation error
   resource function post student(@http:Payload Student student) returns StudentCreated|ValidationError {
       if student.name.length() == 0 || student.email.length() == 0 {
           log:printWarn("Student name or email is not present", student = student);
           return <ValidationError>{
               body: {
                   'error: {
                       code: "INVALID_NAME",
                       message: "Student name and email are required"
                   }
               }
           };
       }
 
 
       log:printDebug("Adding new Student", student = student);
       student.studentId = uuid:createType1AsString();
       self.students[<string>student.studentId] = student;
       log:printInfo("Added new student", student = student);
 
       string studentUrl = string `/students/${<string>student.studentId}`;
       return <StudentCreated>{
           headers: {
               location: studentUrl
           }
       };
   }
 
   # Update Student record
   #
   # + student - Student record updated
   # + return - A Student updated response or an error if the Student is invalid
   resource function put student(@http:Payload Student student) returns StudentUpdated|ValidationError {
       if student.studentId is ("") || !self.students.hasKey(<string>student.studentId) {
           log:printWarn("Invalid student provided for update", student = student);
           return <ValidationError>{
               body: {
                   'error: {
                       code: "INVALID_Student",
                       message: "Invalid Student"
                   }
               }
           };
       }
 
       log:printInfo("Updating student", student = student);
       self.students[<string>student.studentId] = student;
       return <StudentUpdated>{};
   }
 
   # Deletes Student record
   #
   # + number - Student number
   # + return - Deleted Student record or a validation error
   resource function delete students/[string number]() returns Student|ValidationError {
       if !self.students.hasKey(<string>number) {
           log:printWarn("Invalid Student number to be deleted", number = number);
           return {
               body: {
                   'error: {
                       code: "INVALID_number",
                       message: "Invalud Student number"
                   }
               }
           };
       }
 
       log:printDebug("Deleting Student record", number = number);
       Student removed = self.students.remove(number);
       log:printDebug("Deleted Student", Student = removed);
       return removed;
   }
}

